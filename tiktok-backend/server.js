import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import Videos from './model/tiktokCards.js';

// App config
const app = express();
const port = process.env.PORT || 8000;

// Middleware
app.use(express.json());
app.use(cors());

// DB config
const connection_url = 'mongodb+srv://rivz-admin:vOKKstkDjmrnPftU@tiktokcluster.dszw4.mongodb.net/tiktokDB?retryWrites=true&w=majority';

mongoose.connect(connection_url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('DB is connected'));

// API endpoint
app.get('/', (req, res) => res.status(200).send('Welcome to TikTok Clone Backend by RivZ'));

app.get('/tiktok/videos', (req, res) => {
    Videos.find((err, data) => {
        if(err) {
            res.status(500).send(err);
        } else {
            res.status(200).send(data)
        }
    })
})

app.post('/tiktok/video', (req, res) => {
    const tkVideos = req.body;
    
    Videos.create(tkVideos, (err, data) => {
        if(err) {
            res.status(500).send(err);
        } else {
            res.status(201).send(data)
        }
    })
})

// Listener
app.listen(port, () => console.log(`Listening on port:${port}`));