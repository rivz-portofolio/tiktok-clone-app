import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://tiktok-clone-rivz.herokuapp.com'
})

export default instance;