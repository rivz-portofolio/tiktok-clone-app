import { useEffect, useState } from 'react';
import './App.css';
import Video from './Video';
import axios from './axios';

function App() {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    const fetchData = async function() {
      const res = await axios.get('/tiktok/videos')

      setVideos(res.data)
    }

    fetchData()

  }, [])

  return (
    <div className="app">
        <div className='app_videos'>
          {
            videos.map(({url, channel, description, song, likes, messages, shares}, index) => (
              <Video
                key={index}
                url={url}
                channel={channel}
                description={description}
                song={song}
                likes={likes}
                messages={messages}
                shares={shares} />
            ))
          }
        </div>
    </div>
  );
}

export default App;